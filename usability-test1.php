<?php
include ('top.php');
?>

<section id="main">
    <h1 class='collective'>Usability Testing Round 1</h1>
    <h2 class='subjects'>Reid's Test</h2> 
    <iframe class='usability' width="560" height="315" src="https://www.youtube.com/embed/2NtI6a8XZrE" 
            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    <h2 class='subjects'>Selvi's Test</h2> 
    <iframe class='usability' width="560" height="315" src="https://www.youtube.com/embed/L3fRI-u_mNM" 
            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    <h2 class='artists-feature'>What did we change?</h2>
    <p class="clear-changes">Added a hover to the tool bar icon so it could be more clear it was a hyper-link (when active).</p>

    <img class='changes' id='extra' src="images/toolbar2.png" alt="">
    <img class='changes' id='arrow' src="images/arrow.png" alt=""> 
    <img class='changes' id='extra' src="images/toolbar1.png" alt="">

    <p class="clear-changes">The form would not work if you put a question mark in the question section, so we fixed that.</p>

    <img class='changes' id='extra' src="images/mark.png" alt="">
    <img class='changes' id='arrow' src="images/arrow.png" alt=""> 
    <img class='changes' id='extra' src="images/question.png" alt="">


    <h2 class='artists-feature'>Documentation</h2>
    <p class="clear-changes">Here is the <a href="test-script.pdf" target="_blank">Test Script</a> and the <a href="test-reid-selvi.pdf" target="_blank">Summary of Test</a>.</p>

</section> 


<?php
include "footer.php";
?>
</body>
</html>