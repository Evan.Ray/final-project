<?php
include ('top.php');
?>
<!DOCTYPE HTML>
<html>
    <body>
        <article id="main">

            <title>Featured Artists</title>
            <h1 class='collective'>New Vision Featured Artists</h1>

            <p class="featured-description">We make music because we love music.  
                This page is paying homage to our favorite artist (no specific order).</p>

            <h2 class='featured'>Top Artist Choices</h2>

            <h3 class='check-out'>(Go listen now if you've been sleeping)</h3>

            <div class="col-md-7">
                <h2 class='artists-feature'>A$AP Rocky</h2>    
                <p class="feat-bio">Rakim Mayers (born October 3, 1988), better known by his stage 
                    name ASAP Rocky (stylized as A$AP Rocky), is an American rapper, 
                    songwriter, record producer, and actor. He is a member of the hip hop group A$AP Mob, 
                    from which he adopted his nickname.</p>
                <h3 class="description">Our top five songs picks (explicit):</h3> 
                <ul class='feat-songs'>
                    <li><a href="https://www.youtube.com/watch?v=KfVIRigPyws" target="_blank">Everyday ft. Miguel, Mark Ronson & Rod Stewart</a></li>
                    <li><a href="https://www.youtube.com/watch?v=fLCf-URqIf0" target="_blank">Goldie</a></li>
                    <li><a href="https://www.youtube.com/watch?v=kxWNNHCrcFg" target="_blank">Peso</a></li>
                    <li><a href="https://www.youtube.com/watch?v=BNzc6hG3yN4" target="_blank">A$AP Forever ft. Moby</a></li>
                    <li><a href="https://www.youtube.com/watch?v=DEKeVrUB_uQ" target="_blank">M'$ ft. Lil Wayne</a></li>
                </ul>
            </div>

            <div class="col-md-5">
                <img src="images/asap-rocky.jpg" alt="" class="members">
            </div>

            <div class="col-md-7">
                <h2 class='artists-feature'>Chance The Rapper</h2>  
                <p class="feat-bio">Chancelor Jonathan Bennett (born April 16, 1993), 
                    known professionally as Chance the Rapper, is an American rapper, 
                    singer, songwriter, actor, and record producer from the West Chatham 
                    neighborhood of Chicago, Illinois. In 2013, he began to gain recognition 
                    following the release of his second mixtape, Acid Rap. In May 2016, Bennett 
                    released his third mixtape Coloring Book to critical acclaim. 
                    It earned him three Grammy Awards, including Best Rap Album. Coloring Book became the 
                    first streaming-only album to receive a nomination for, and win, a Grammy.</p>
                <h3 class="description">Our top five songs picks (explicit):</h3> 
                <ul class='feat-songs'>
                    <li><a href="https://www.youtube.com/watch?v=bbq016ZOWYw" target="_blank">Brain Cells</a></li>
                    <li><a href="https://www.youtube.com/watch?v=CwnZI01YdXE" target="_blank">Interlude (That's Love)</a></li>
                    <li><a href="https://www.youtube.com/watch?v=it1e4JvXuCE" target="_blank">Yolo</a></li>
                    <li><a href="https://www.youtube.com/watch?v=Av8sn7BXLxE" target="_blank">Juice</a></li>
                    <li><a href="https://www.youtube.com/watch?v=-aJ3l02_6L4" target="_blank">Smoke Break (ft. Future)</a></li>
                </ul>
            </div>

            <div class="col-md-5">
                <img src="images/chance.jpg" alt="" class="members">
            </div>

            <div class="col-md-7">
                <h2 class='artists-feature'>Danny Brown</h2>  
                <p class="feat-bio">Daniel Dewan Sewell (born March 16, 1981), 
                    known professionally as Danny Brown, is an American rapper. 
                    He is best known for his individuality, being described by MTV as 
                    "one of rap's most unique figures in recent memory". In 2010, 
                    after amassing several mixtapes, Brown released his debut studio album, 
                    The Hybrid. Brown began to gain major recognition after the release of 
                    his second studio album, XXX, which received critical acclaim and earned 
                    him such accolades as Spin, as well as Metro Times "Artist of the Year".</p>
                <h3 class="description">Our top five songs picks (explicit):</h3>     
                <ul class='feat-songs'>
                    <li><a href="https://www.youtube.com/watch?v=d0s0XHVUGF0" target="_blank">25 Bucks (ft. Purity Ring)</a></li>
                    <li><a href="https://www.youtube.com/watch?v=LVyGxlgeAjc" target="_blank">When It Rain</a></li>
                    <li><a href="https://www.youtube.com/watch?v=spfsdpuvUyQ" target="_blank">Really Doe (ft. Kendrick Lamar, Ab-Soul & Earl Sweatshirt)</a></li>
                    <li><a href="https://www.youtube.com/watch?v=NHfWY0is3rE" target="_blank">Grown Up</a></li>
                    <li><a href="https://www.youtube.com/watch?v=7L4JnAuW00k" target="_blank">Ain't it Funny</a></li>
                </ul>
            </div>

            <div class="col-md-5">
                <img src="images/danny-brown.jpg" alt="" class="members">
            </div>

            <div class="col-md-7">
                <h2 class='artists-feature'>Earl Sweatshirt</h2>  
                <p class="feat-bio"> Thebe Neruda Kgositsile (born February 24, 1994), 
                    better known by his stage name Earl Sweatshirt, is an American 
                    rapper and record producer from Los Angeles, California son of the black nationalist 
                    poet Keorapetse Kgositsile. In February 2012, Earl rejoined 
                    Odd Future and started producing new music. Earl released his 
                    debut studio album, Doris in August 2013. His second album, 
                    I Don't Like Shit, I Don't Go Outside: An Album by Earl Sweatshirt 
                    followed in March 2015. Both received widespread critical acclaim. 
                    He is currently signed to Columbia Records as well as his independent label Tan Cressida.</p>
                <h3 class="description">Our top five songs picks (explicit):</h3>     
                <ul class='feat-songs'>
                    <li><a href="https://www.youtube.com/watch?v=FCbWLSZrZfw" target="_blank">Chum</a></li>
                    <li><a href="https://www.youtube.com/watch?v=0FcDXL5Aw0o" target="_blank">Hive (ft. Vince Staples & Casey Veggies)</a></li>
                    <li><a href="https://www.youtube.com/watch?v=-UVSb1yP3rs" target="_blank">Molasses (ft. RZA)</a></li>
                    <li><a href="https://www.youtube.com/watch?v=8Cc-EFNA1sQ" target="_blank">Hoarse</a></li>
                    <li><a href="https://www.youtube.com/watch?v=tZ5Mu2gs-M8" target="_blank">Grief</a></li>
                </ul>
            </div>

            <div class="col-md-5">
                <img src="images/earl.jpg" alt="" class="members">
            </div>

            <div class="col-md-7">
                <h2 class='artists-feature'>Drake</h2>    
                <p class="feat-bio">Aubrey Drake Graham (born October 24, 1986)
                    is a Canadian rapper, singer, songwriter, record producer, actor, 
                    and entrepreneur. Drake initially gained recognition as an actor 
                    on the teen drama television series Degrassi: The Next Generation 
                    in the early 2000s. Intent on pursuing a career as a rapper, he departed 
                    the series in 2007 following the release of his debut mixtape, Room for Improvement. 
                    He released two further independent projects, Comeback Season and So Far Gone, before 
                    signing to Lil Wayne's Young Money Entertainment in June 2009</p>
                <h3 class="description">Our top five songs picks (explicit):</h3>     
                <ul class='feat-songs'>
                    <li><a href="https://www.youtube.com/watch?v=U9BwWKXjVaI" target="_blank">Nice For What</a></li>
                    <li><a href="https://www.youtube.com/watch?v=xpVfcZ0ZcFM" target="_blank">God's Plan</a></li>
                    <li><a href="https://www.youtube.com/watch?v=SZDmuHSqwtg" target="_blank">Used to (ft. Lil Wayne)</a></li>
                    <li><a href="https://www.youtube.com/watch?v=7LnBvuzjpr4" target="_blank">Energy</a></li>
                    <li><a href="https://www.youtube.com/watch?v=F33o_AOyCPk" target="_blank">Tuscan Leather</a></li>
                </ul>
            </div>

            <div class="col-md-5">
                <img src="images/drake.jpg" alt="" class="members">
            </div>

            <div class="col-md-7">
                <h2 class='artists-feature'>Eminem</h2>    
                <p class="feat-bio">Marshall Bruce Mathers III (born October 17, 1972),
                    known professionally as Eminem, is an American rapper, songwriter, 
                    record producer, record executive, and actor. Eminem is the best-selling artist 
                    of the 2000s in the United States. Throughout his career, he has had 10 number-one 
                    albums on the Billboard 200 and five number-one singles on the Billboard Hot 100. 
                    With 47.4 million albums sold in the US and 220 million records globally, he is among 
                    the world's best-selling artists of all time and is consistently cited as one of the 
                    greatest and most influential artists in any genre.</p>
                <h3 class="description">Our top five songs picks (explicit):</h3>     
                <ul class='feat-songs'>
                    <li><a href="https://www.youtube.com/watch?v=1wYNFfgrXTI" target="_blank">When I'm Gone</a></li>
                    <li><a href="https://www.youtube.com/watch?v=ytQ5CYE1VZw" target="_blank">'Till I Collapse</a></li>
                    <li><a href="https://www.youtube.com/watch?v=S9bCLPwzSC0" target="_blank">Mockingbird</a></li>
                    <li><a href="https://www.youtube.com/watch?v=5ja-mHeYAKM" target="_blank">Drug Ballad</a></li>
                    <li><a href="https://www.youtube.com/watch?v=lgT1AidzRWM" target="_blank">Beautiful</a></li>
                </ul>
            </div>

            <div class="col-md-5">
                <img src="images/eminem.jpg" alt="" class="members">
            </div>

    </body>

    <div class="col-md-7">
        <h2 class='artists-feature'>Flatbush Zombies</h2>    
        <p class="feat-bio">Flatbush Zombies is an American hip hop group from the Flatbush section of Brooklyn, 
            New York City, formed in 2010. The group is composed of rappers Meechy Darko, Zombie Juice and Erick 
            Arc Elliott, with Elliott also serving as their regular record producer. The trio are part of the East 
            Coast hip hop movement known as "Beast Coast", which also consists of fellow Brooklyn-based rap groups 
            The Underachievers and Pro Era.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=YPYsfMd16JQ" target="_blank">Trade-Off</a></li>
            <li><a href="https://www.youtube.com/watch?v=DzLJCu95RKI" target="_blank">U&I (ft. Dia)</a></li>
            <li><a href="https://www.youtube.com/watch?v=ix8kLAPQsjE" target="_blank">Face-Off</a></li>
            <li><a href="https://www.youtube.com/watch?v=MZ924VGx5n0" target="_blank">Headstone</a></li>
            <li><a href="https://www.youtube.com/watch?v=kfzRXseSBIM" target="_blank">Palm Trees</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/flatbush-zombies.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>Childish Gambino</h2>    
        <p class="feat-bio">Donald McKinley Glover Jr. (born September 25, 1983) is an American actor, 
            comedian, writer, director, producer, singer, songwriter, rapper, and DJ. He raps under the stage 
            name Childish Gambino and performs as a DJ under the name mcDJ.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=qL1B_r9nC9k" target="_blank">Bonfire</a></li>
            <li><a href="https://www.youtube.com/watch?v=f7plhrsQSEE" target="_blank">Backpackers</a></li>
            <li><a href="https://www.youtube.com/watch?v=Kp7eSUU9oy8" target="_blank">Redbone</a></li>
            <li><a href="https://www.youtube.com/watch?v=tG35R8F2j8k" target="_blank">3005</a></li>
            <li><a href="https://www.youtube.com/watch?v=jx96Twg-Aew" target="_blank">Sober</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/gambino.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>Goldlink</h2>    
        <p class="feat-bio">DeAnthony Carlos, better known by his stage name GoldLink, 
            is an American rapper. In 2014, he released his debut mixtape, The God Complex, 
            which received critical acclaim. In June 2015, he was chosen as part of the XXL Freshman Class. 
            In October 2015, GoldLink released his second mixtape, And After That, We Didn't Talk, which was 
            supported by the singles, "Dance on Me" and "Spectrum". His debut studio album, At What Cost, was 
            released on March 24, 2017 to positive reviews. The album's lead single, "Crew", has peaked at number 
            45 on the US Billboard Hot 100 chart, as well as earned him a Grammy nomination.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=m-zeV_s3bfY" target="_blank">Herside Story</a></li>
            <li><a href="https://www.youtube.com/watch?v=Ya4jsYQAne0" target="_blank">Palm Trees</a></li>
            <li><a href="https://www.youtube.com/watch?v=993ePz8MS88" target="_blank">Roll Call</a></li>
            <li><a href="https://www.youtube.com/watch?v=nhNqbe6QENY" target="_blank">Crew</a></li>
            <li><a href="https://www.youtube.com/watch?v=tPJXjGEHsfw" target="_blank">Kokamoe Freestyle</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/goldlink.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>Isaiah Rashad</h2>  
        <p class="feat-bio">Isaiah Rashad McClain (born May 16, 1991), is an American hip hop recording 
            artist and record producer from Chattanooga, Tennessee. Rashad began taking rapping seriously 
            in tenth grade, where he and his friends would record on laptops. He spent the next few years 
            recording at local recording studios. His first big break would be touring with rappers Juicy J, 
            Joey Badass and Smoke DZA among others, on the 2012 Smoker's Club Tour. He is also a founding 
            member of the Chattanooga hip hop collective The House along with fellow Chattanooga rapper TUT 
            and a member of the Chicago hip hop collective The Village along with artist Kembe X, Alex Wiley and more.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=yrHQka9XchA" target="_blank">West Savannah (ft. SZA)</a></li>
            <li><a href="https://www.youtube.com/watch?v=sc7OgcUr-NE" target="_blank">Soliloquy</a></li>
            <li><a href="https://www.youtube.com/watch?v=nppKPgdc_u0" target="_blank">4r Da Squaw</a></li>
            <li><a href="https://www.youtube.com/watch?v=QJksTRgY8-c" target="_blank">Free Lunch</a></li>
            <li><a href="https://www.youtube.com/watch?v=L3YzYRgrIRU" target="_blank">Heavenly Father</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/isiah.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>J.I.D.</h2>  
        <p class="feat-bio">Destin Route (born October 31, 1990), better known by his stage name 
            J.I.D, is an American rapper and songwriter from Atlanta, Georgia. 
            He is signed to J. Cole's Dreamville Records. His debut album, The Never Story, 
            was released on March 10, 2017.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=6eFcSesrP6A" target="_blank">Never</a></li>
            <li><a href="https://www.youtube.com/watch?v=cgtY0y861zI" target="_blank">8701 (ft. 6LACK)</a></li>
            <li><a href="https://www.youtube.com/watch?v=SEG92Z_NGJE" target="_blank">EdEddnEddy</a></li>
            <li><a href="https://www.youtube.com/watch?v=UxinGFri13s" target="_blank">Somebody</a></li>
            <li><a href="https://www.youtube.com/watch?v=6GVyiF-Yce8" target="_blank">Hereditary</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/j.i.d.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>J. Cole</h2> 
        <p class="feat-bio">Jermaine Lamarr Cole (born January 28, 1985) is a American hip hop 
            recording artist and record producer.Self-taught on piano, Cole also acts as a producer 
            alongside his hip-hop career, producing singles for artists such as Kendrick Lamar and 
            Janet Jackson, as well as handling the majority of the production in his own projects.
            He has also developed other ventures, including Dreamville Records, as well as a non-profit
            organization called the Dreamville Foundation. In January 2015, Cole decided to house 
            single mothers rent-free at his childhood home in Fayetteville, North Carolina.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=wvFDXV0VBg4" target="_blank">False Prophets</a></li>
            <li><a href="https://www.youtube.com/watch?v=FOPgg7qqlcA" target="_blank">Neighbors</a></li>
            <li><a href="https://www.youtube.com/watch?v=y-vQ_VsTkn0" target="_blank">Lost Ones</a></li>
            <li><a href="https://www.youtube.com/watch?v=VoGqSiZ-Wp4" target="_blank">Is She Gon Pop</a></li>
            <li><a href="https://www.youtube.com/watch?v=eCGV26aj-mM" target="_blank">Wet Dreams</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/jcole.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>Joey BadA$$</h2>   
        <p class="feat-bio">Jo-Vaughn Virginie Scott (born January 20, 1995),
            better known by his stage name Joey Badass (stylized as Joey Bada$$), 
            is an American rapper, actor, and record producer. Raised in Brooklyn, 
            New York, he is a founding member of the hip-hop collective Pro Era, 
            with whom he has released three mixtapes. His debut studio album, B4.DA.$$, 
            was released January 20, 2015. In July 2016, he made his television debut 
            on the USA Network series Mr. Robot. His second studio album ALL-AMERIKKKAN BADA$$ was released on April 7, 2017.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=RLnA25dVzrQ" target="_blank">Devastated</a></li>
            <li><a href="https://www.youtube.com/watch?v=stoLqWXsIOY" target="_blank">Paper Trails</a></li>
            <li><a href="https://www.youtube.com/watch?v=TeQW-9Cg8qs" target="_blank">Land Of The Free</a></li>
            <li><a href="https://www.youtube.com/watch?v=DDWAk8-leVA" target="_blank">Survival Tactics (ft. Capital STEEZ)</a></li>
            <li><a href="https://www.youtube.com/watch?v=ekDBhzfwOPY" target="_blank">King's Dead vs XXXTENTACION Freestyle</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/joey-badass.jpeg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>Joyner Lucas</h2> 
        <p class="feat-bio">Gary Lucas (born August 17, 1988) better known by his 
            stage name Joyner Lucas, is an American rapper from Worcester, 
            Massachusetts currently signed to Atlantic Records. He garnered 
            widespread exposure and critical acclaim after the release of his single 
            "Ross Capicchioni" in 2015. In 2017, he released his fourth mixtape 
            (and first on a major label), 508-507-2209.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=E-u9uo0laA8" target="_blank">Stranger Things (ft. Chris Brown)</a></li>
            <li><a href="https://www.youtube.com/watch?v=2eUDm2_ZSwc" target="_blank">Gucci Gang (Remix)</a></li>
            <li><a href="https://www.youtube.com/watch?v=xzHJ2aZR6e4" target="_blank">I Don't Die (ft. Chris Brown)</a></li>
            <li><a href="https://www.youtube.com/watch?v=ZFy7RdZWwj8" target="_blank">Bank Account (Remix)</a></li>
            <li><a href="https://www.youtube.com/watch?v=43gm3CJePn0" target="_blank">I'm Not Racist</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/joyner.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>Kendrick Lamar</h2>  
        <p class="feat-bio">Kendrick Lamar Duckworth (born June 17, 1987) is an American 
            rapper and songwriter. Raised in Compton, California, Lamar embarked on his musical 
            career as a teenager under the stage name K-Dot, releasing a mixtape that garnered local 
            attention and led to his signing with indie record label Top Dawg Entertainment (TDE). 
            Lamar has received a number of accolades over the course of his career, including twelve 
            Grammy Awards. In early 2013, MTV named him the number one "Hottest MC in the Game", on 
            their annual list. Time named him one of the 100 most influential people in the world in 2016.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=Dlh-dzB2U4Y" target="_blank">Loyalty (ft. Rihanna)</a></li>
            <li><a href="https://www.youtube.com/watch?v=aukDlZZnXUU" target="_blank">Money Trees</a></li>
            <li><a href="https://www.youtube.com/watch?v=10yrPDf92hY" target="_blank">m.A.A.d city</a></li>
            <li><a href="https://www.youtube.com/watch?v=GDxkMYVDB5w" target="_blank">Hol' Up</a></li>
            <li><a href="https://www.youtube.com/watch?v=_yijmjOfSUc" target="_blank">Ronald Reagan Era</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/kendrick.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>Lil Wayne</h2>  
        <p class="feat-bio">Dwayne Michael Carter Jr. (born September 27, 1982),
            known professionally as Lil Wayne, is an American rapper. In 1991, at the age of nine, 
            Lil Wayne joined Cash Money Records as the youngest member of the label. 
            Lil Wayne is also the Chief Executive Officer (CEO) of his own label imprint, 
            Young Money Entertainment, which he founded in 2005. On September 27, 2012, Lil Wayne 
            passed Elvis Presley as the male with the most entries on the 
            Billboard Hot 100 chart, with 109 songs.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=y8Gf4-eT3w0" target="_blank">How To Love</a></li>
            <li><a href="https://www.youtube.com/watch?v=c7tOAGY59uQ" target="_blank">6 Foot 7 Foot</a></li>
            <li><a href="https://www.youtube.com/watch?v=UROLAsyc_KU" target="_blank">Amili</a></li>
            <li><a href="https://www.youtube.com/watch?v=2IH8tNQAzSs" target="_blank">Lollipop (ft. Static)</a></li>
            <li><a href="https://www.youtube.com/watch?v=eC2pJrbfig4" target="_blank">Blunt Blowin</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/lil-wayne.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>Mick Jenkins</h2>  
        <p class="feat-bio">Jayson Mick Jenkins (born April 16, 1991), known professionally 
            as Mick Jenkins, is an American hip hop recording artist. Based in Chicago, he is 
            signed to Cinematic Music Group. His debut album, The Healing Component was 
            released on September 23, 2016.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=F-4KiszXxHM" target="_blank">Jazz</a></li>
            <li><a href="https://www.youtube.com/watch?v=ITucGzGrEy4" target="_blank">514</a></li>
            <li><a href="https://www.youtube.com/watch?v=5xLEGmijvMg" target="_blank">Martyrs</a></li>
            <li><a href="https://www.youtube.com/watch?v=GigIohlYqEI" target="_blank">P's & Q's</a></li>
            <li><a href="https://www.youtube.com/watch?v=NKF4tkhTykM" target="_blank">Vibe</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/mick-jenkins.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>Russ</h2>  
        <p class="feat-bio">Russell Vitale (born September 26, 1992), known by his 
            stage name Russ, is an American hip hop recording artist, 
            singer-songwriter and record producer. He is known for his singles 
            "What They Want" and "Losin Control", which peaked respectively at 
            number 83 and 63 on the US Billboard Hot 100. He is part of the Diemon Crew, 
            a rap group. His debut Columbia studio album There's Really a Wolf was 
            released on May 5, 2017, and, on April 18, 2018, it was certified Platinum.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=oorK4RPgZ8Q" target="_blank">What They Want</a></li>
            <li><a href="https://www.youtube.com/watch?v=FMW6ezkJN9Y" target="_blank">Me You</a></li>
            <li><a href="https://www.youtube.com/watch?v=DH0ln0xzjfg" target="_blank">Sore Losers</a></li>
            <li><a href="https://www.youtube.com/watch?v=1vvBf9E-2ek" target="_blank">Tsunami</a></li>
            <li><a href="https://www.youtube.com/watch?v=y78n5kVYqPo" target="_blank">Ride Slow</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/russ.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>Tyler, The Creator</h2>  
        <p class="feat-bio">yler Gregory Okonma (born March 6, 1991), better known as Tyler, the Creator, 
            is an American rapper, record producer, and music video director.
            He rose to prominence as the leader and co-founder of the alternative 
            hip hop collective Odd Future and has rapped on and produced songs for 
            nearly every Odd Future release. Okonma creates all the artwork for 
            the group's releases and also designs the group's clothing and 
            other merchandise.</p>
        <h3 class="description">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=EZE62LpaqHg" target="_blank">See You Again</a></li>
            <li><a href="https://www.youtube.com/watch?v=XSbZidsgMfw" target="_blank">Yonkers</a></li>
            <li><a href="https://www.youtube.com/watch?v=jQNazsWnQKU" target="_blank">OKRA</a></li>
            <li><a href="https://www.youtube.com/watch?v=FUXX55WqYZs" target="_blank">Who Dat Boy</a></li>
            <li><a href="https://www.youtube.com/watch?v=3lDqMx4rmFU" target="_blank">IFHY</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/tyler.jpg" alt="" class="members">
    </div>

    <div class="col-md-7">
        <h2 class='artists-feature'>Vince Staples</h2>     
        <p class="feat-bio">Vincent Jamal Staples (born July 2, 1993) is an American 
            rapper from the Ramona Park area of North Long Beach, California.
            He is a member of the hip hop trio Cutthroat Boyz alongside fellow 
            Californian rappers Aston Matthews and Joey Fatts. Staples was once a close 
            associate of Odd Future, in particular Mike G and Earl Sweatshirt. 
            He is currently signed to Blacksmith Records, ARTium Recordings and Def Jam Recordings.</p>
        <h3 class="description-song">Our top five songs picks (explicit):</h3>     
        <ul class='feat-songs'>
            <li><a href="https://www.youtube.com/watch?v=YLpH78k5A9A" target="_blank">745</a></li>
            <li><a href="https://www.youtube.com/watch?v=mb6Jc4juSF8" target="_blank">Norf Norf</a></li>
            <li><a href="https://www.youtube.com/watch?v=0l9kzS_B7gg" target="_blank">Big Fish</a></li>
            <li><a href="https://www.youtube.com/watch?v=5OAYMMod9Wo" target="_blank">Señorita</a></li>
            <li><a href="https://www.youtube.com/watch?v=C6iAzyhm0p0" target="_blank">Yeah Right</a></li>
        </ul>
    </div>

    <div class="col-md-5">
        <img src="images/vince.jpg" alt="" class="members">
    </div>
</article>
 
</html>

<?php
include ('footer.php');
?>