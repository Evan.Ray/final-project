 <?php
include ('top.php');
?>

<section id="main">
    <h1 class='collective'>Welcome to New Vision Collective Hip-Hop Entertainment</h1>
    <h2 class='who-dis'>Who Are We?</h2>
    <img src="images/new-vision2.png" alt="" class="flower">
    <p id='intro'>&nbsp;&nbsp;&nbsp;&nbsp; The New Vision collective is a group of artists that create Hip-Hop music of multiple Hip-Hop subgenres.
       The members of New Vision have been mostly connected through one of our rappers ZEWZ, being the only member to have 
       lived on the East coast and West coast.  New Vision currently has five active members being ZEWZ, Spitt, the Kidd
       GSWAN 28, Ivana Djiya and Evan Ray.  Ivana Djiya and Evan Ray both live on the East Coast in Burlington VT, while the
       remaining three members live in Bend, Oregon.</p>
       <p id='intro'>&nbsp;&nbsp;&nbsp;&nbsp; The collective formed naturally through our members love of making music
       with mutual connections.  As independent artists there is a lot of work that goes into making music, as a collective it is 
       much easier.  Making Hip-Hop music involves production, mixing, rapping, mastering, engineering, promoting, creating cover art
       and the list goes on.  As a collective we can divide up these jobs with specialization of labor and get a more efficient and higher quality result.
       We also have a more diverse and versatile sound ability as a group, because all of our members have a different types of sound.
       We have created this web page to promote our music and to give our audience more information about our organization. 
    </p>
    
    <h2 class='who-dis'>New Vision Mission</h2>
    <p id='intro'>&nbsp;&nbsp;&nbsp;&nbsp; Our mission is to create good music to spread love and promote positivity while 
        preserving the original integrity of Hip-Hop.  Not just mumble rapping, talking about money and women, but to actually
        make music with artistic value.</p>

</section> 

<?php
include "footer.php";
?>
    </body>
</html>
