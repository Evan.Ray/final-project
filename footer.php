<!-- ######################     Start of Footer   ########################## -->
<footer>
    <p class="footer">Website Created By: Evan Ray</p>
    <a href="usability-test1.php" class="footerText">Usability Test 1</a>
    <a href="usability-test2.php" class="footerText">Usability Test 2</a>
    <a href="sources.php" class="footerText">Sources</a>
    <a href="style.php" class="footerText">Style Choice Explanation</a>
    <br>
    <br>
    <a href="#backTop" class="footerText">Back to Top</a>
</footer>
<!-- ######################     End of Footer   ############################ -->