<?php
include ('top.php');
?>

<section id="main">
    <h1 class='collective'>Usability Testing Round 2</h1>
    <h2 class='subjects'>Sam's Test</h2> 
    <iframe class='usability' width="560" height="315" src="https://www.youtube.com/embed/WFiE1YoUhow" 
            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    <h2 class='subjects'>Hamza's Test</h2> 
    <iframe class='usability' width="560" height="315" src="https://www.youtube.com/embed/aiWHJU0LnCY" 
            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

    <h2 class='artists-feature'>What did we change?</h2>
    <p class="clear-changes">The navigation needed more clear titles.</p>

    <img class='changes2' id='extra' src="images/nav.png" alt="">
    <div class="margin">
        <img class='changes' id='arrow-down' src="images/arrow-down.png" alt=""> 
        <img class='changes' id='arrow-down' src="images/arrow-down.png" alt=""> 
        <img class='changes' id='arrow-down' src="images/arrow-down.png" alt="">
        <img class='changes' id='arrow-down' src="images/arrow-down.png" alt=""> 
        <img class='changes' id='arrow-down' src="images/arrow-down.png" alt="">        
    </div>
    <img class='changes2' id='extra' src="images/nav2.png" alt="">
    
    <p class="clear-changes">The Top Releases page needed a more obvious description of the content.</p>
    
    <img class='changes2' id='extra' src="images/project-change.png" alt="">

    <div class="margin">
        <img class='changes' id='arrow-down' src="images/arrow-down.png" alt=""> 
        <img class='changes' id='arrow-down' src="images/arrow-down.png" alt=""> 
        <img class='changes' id='arrow-down' src="images/arrow-down.png" alt="">
        <img class='changes' id='arrow-down' src="images/arrow-down.png" alt=""> 
        <img class='changes' id='arrow-down' src="images/arrow-down.png" alt="">        
    </div>
    <img class='changes2' id='extra' src="images/project-post.png" alt="">


    <h2 class='artists-feature'>Documentation</h2>
    <p class="clear-changes">Here is the <a href="test-script.pdf" target="_blank">Test Script</a> and the <a href="test-sam-hamza.pdf" target="_blank">Summary of Test</a>.</p>


</section> 

<?php
include "footer.php";
?>
</body>
</html>