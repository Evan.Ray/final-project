 <?php
include ('top.php');
?>

<section id="main">
    <h1 class='collective'>Sources</h1>
    <ul>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/ASAP_Rocky" target="_blank">https://en.wikipedia.org/wiki/ASAP_Rocky</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Chance_the_Rapper" target="_blank">https://en.wikipedia.org/wiki/Chance_the_Rapper</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Danny_Brown" target="_blank">https://en.wikipedia.org/wiki/Danny_Brown</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Earl_Sweatshirt" target="_blank">https://en.wikipedia.org/wiki/Earl_Sweatshirt</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Drake_(musician)" target="_blank">https://en.wikipedia.org/wiki/Drake_(musician)</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Eminem" target="_blank">https://en.wikipedia.org/wiki/Eminem</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Flatbush_Zombies" target="_blank">https://en.wikipedia.org/wiki/Flatbush_Zombies</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Donald_Glover" target="_blank">https://en.wikipedia.org/wiki/Donald_Glover</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/GoldLink" target="_blank">https://en.wikipedia.org/wiki/GoldLink</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Isaiah_Rashad" target="_blank">https://en.wikipedia.org/wiki/Isaiah_Rashad</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/J.I.D" target="_blank">https://en.wikipedia.org/wiki/J.I.D</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/J._Cole" target="_blank">https://en.wikipedia.org/wiki/J._Cole</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Joey_Badass" target="_blank">https://en.wikipedia.org/wiki/Joey_Badass</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Joyner_Lucas" target="_blank">https://en.wikipedia.org/wiki/Joyner_Lucas</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Kendrick_Lamar" target="_blank">https://en.wikipedia.org/wiki/Kendrick_Lamar</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Lil_Wayne" target="_blank">https://en.wikipedia.org/wiki/Lil_Wayne</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Mick_Jenkins_(rapper)" target="_blank">https://en.wikipedia.org/wiki/Mick_Jenkins_(rapper)</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Russ_(rapper)" target="_blank">https://en.wikipedia.org/wiki/Russ_(rapper)</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Tyler,_the_Creator" target="_blank">https://en.wikipedia.org/wiki/Tyler,_the_Creator</a></li>
        <li class='sources-links'><a href="https://en.wikipedia.org/wiki/Vince_Staples" target="_blank">https://en.wikipedia.org/wiki/Vince_Staples</a></li>        
    </ul>
</section> 

<?php
include "footer.php";
?>
    </body>
</html>