<?php
include ('top.php');
?>

<article id="main">

    <title>Members</title>
    <h1 class='collective'>New Vision Members</h1>

    <div class="col-md-8">
        <h2 class='artists'>ZEWZ</h2>
        <h3 class="description">Artist</h3>
        <p class="bio">The young and talented ZEWZ is a nineteen year old rapper from Bend, Oregon.
        ZEWZ (government name Henry Hochschild) found his love for rapping at a young age and has
        been freestyling for as long as he can remember.  He originally used the rap alias 
        "Blackie-chan" for the duration of his highschool education.  After graduating from Vermont Academy,
        ZEWZ travelled to New Zealand to do some soul searching.  This is where he
        developed his new rap alias ZEWZ.
        </p>
        <p class="bio">ZEWZ was a crucial member in creating the New Vision collective
        because he linked the members of New Vision from the East coast and the West coast being to only member to 
        live in both places during his rap career.  ZEWZ always brings the bars, he has a creative, unique lyrical
        style that continues to progress.  He has dropped two major projects on Sound Cloud, "DAYS IN GOWANDALE" and "Crown tha kid Ep-2017"
        and many singles with features from Evan Ray and Spitt, the Kid, all produced by GSAN 28.
        </p>
    </div>
    
    <div class="col-md-4">
        <img src="images/zewz.jpg" alt="" class="members">
    </div>

    <div class="col-md-8">
        <h2 class='artists'>Evan Ray</h2>
        <h3 class="description">Artist, Rapper</h3>
        <p class="bio">Evan Ray is a 19 year old rapper born in Cornado, California.  He moved to Vermont at the age of
        four, and attended Vermont Academy for his high school education.  It is there where he met ZEWZ, started making music 
        with him.  Evan uses his government as his rap name because he doesn't feel the need to use an alias.
        Evan continues to develop his rapping skills while getting a Business degree at the University of Vermont.
        </p>
        <p class="bio">Evan has appeared on multiple tracks with ZEWZ and Spitt, the Kid.  He recently dropped
        a single on all music media platforms with Ivana Djiya.  Ivana sings the intro, outro, background vocals, and for the bridge of the song.
        The single is called "i'm fine" and is regarded as Evan's debut single (produced by Khronos Beats). Evan also 
        appears on multiple singles with ZEWZ and Spitt, the Kid.  Though Evan considers "i'm fine" to be his debut
        single, he also has several singles on SoundCloud.
        </p>
    </div>    

    <div class="col-md-4">
        <img src="images/evan.jpg" alt="" class="members">
    </div>
        
    <div class="col-md-8">
        <h2 class='artists'>GSWAN 28</h2>
        <h3 class="description">Artist, Rapper, Producer, Audio Engineer</h3>
        <p class="bio">
            GSWAN 28 (government Gavan Wilhite) is a crucial and versatile member of New Vision.
            He is the only official member that produces, mixes, masters and engineers audio.  GSAW 
            produces the majority of the beats for New Vision, and engineers most of the final production.
            GSAWN is also from Bend, Oregon which is how he met ZEWZ.  GSAW accompanied ZEWZ to New Zealand 
            this past year to make music and see the world.
        </p>
        <p class="bio">
            GSWAN doesn't only produce and engineer, he also raps on his own beats.  He has multiple singles 
            with ZEWZ on SoundCloud.  Lastly, GSWAN has dropped one project titled "28... A PROJECT PROD - GSWAN" on SoundCloud.
            GSWAN's style is definitely regarded as new wave sound.
        </p>
    </div> 

    <div class="col-md-4">
        <img src="images/gswan.jpg" alt="" class="members">
    </div>
   
    <div class="col-md-8">
        <h2 class='artists'>Spitt, the Kid</h2>
        <h3 class="description">Artist, Rapper</h3>
        <p class="bio">Spitt, the kid is a 20 year old rapper from Bend, Oregon.  He has developed his own unique style 
        of rapping.  He spits certified bars and is getting increasingly talented at flowing melodically.
        Spitt has features on ZEWZ's tracks and is also featured on ZEWZ's track with Evan Ray.
        </p>
        <p class="bio">Spitt has dropped two projects on SoundCloud.  In 2017, Spitt dropped 
            a Mixtape by the name of "The Unheard Voices" and later in the year dropped an EP called
            "WestSide Villains".  Spitt does not actually consider himself a rapper, he states
            "I am not rapper, I am an artist".
    </div>   

    <div class="col-md-4">
        <img src="images/spitt.jpg" alt="" class="members">
    </div>

    <div class="col-md-8">
        <h2 class='artists'>Ivana Djiya</h2>
        <h3 class="description">Artist, Singer, Photographer, Model, Fashion Designer</h3>
        <p class="bio">Ivana Djiya is the latest edition to the New Vision team.  After being featured on Evan Ray's
        single, "i'm fine" the team decided she must be recruited.  Ivana is a fashion goddess, she is all about
        style and always rocks the freshest gear.  She is a 19 year old Sophomore at UVM, originally from Philadelphia.
        </p>
        <p class="bio">Ivana has an amazing voice and although she considers herself mainly a photographer, her vocals will boost the
           quality level of most any track.  She writes her own songs, skates, models, sings, and much more. 
           She is an all star and your dream girl at the same time.
        </p>
    </div>   

    <div class="col-md-4">
        <img src="images/ivana.jpg" alt="" class="members">
    </div>
    
</article>


<?php
include('footer.php');
?>
</body>
</html>