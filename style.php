<?php
include ('top.php');
?>

<section id="main">
    <h1 class='collective'>Style Choice Explanation</h1>
    <img src="images/flower.jpg" alt="" class="flower">
    <p id='intro-style'>&nbsp;&nbsp;&nbsp;&nbsp; I chose this style because it stems from a original New Vision graphic design.
        I like the specific piece a lot so I used the image as a background for the header, took the 
        caption and made the background transparent for my header, and I used the colors 
        all throughout the page for a consistent color scheme.  This is the original 
        image to the right. If you notice of the home page there is another original 
        New Vision graphic design featuring characters of Naruto. Naruto is a repeating
        theme for New Vision so I used the Sharingan eye from the show as my tool-bar icon.</p>
        
    <p id='intro-style'>&nbsp;&nbsp;&nbsp;&nbsp; I put a gradient background with a blue border around the content. The blue 
        border has slight opacity, all colors taken from the photo to the right.
        I utilized the colors by uploading the photo to a rbga calculator, then I
        simply clicked on the color I wanted the value for. In one of my usability test the late subject,
        Hamza said the color scheme would have diminished utility for him, but the other three 
        test subjects enjoyed the color scheme so I am content. I also personally really like this 
        color scheme, I like that it is bright and artistic because it fits the content.
    </p>
    <h2 class='artists'>Special Features</h2>
    <ul class='feat-songs'>
        <li>Hover effect over New Vision title header, gradient with opacity.</li>
        <li>The New Vision image in the header is a link to the Home page.</li>
        <li>The tool-bar icon in the top left is a link to the Home page with a hover active border.</li>
        <li>The is a back to top hyper link on the bottom of the page.</li>
        <li>Every song suggestion is a link to the youtube audio/visual.</li>
        <li>Submit button has a nice hover effect.</li>
        <li>Every SoundCloud project and single on the Top Releases page is embedded.</li> 
        <li>All links in footer have slightly transparent hover backgrounds.</li>
        <li>Navigation collapses when the page is under a certain size.</li>     
    </ul>
</section>
<?php
include('footer.php');
?>
</body>
</html>